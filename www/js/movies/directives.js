﻿/// <reference path="../angular.js" />
/// <reference path="module.js" />

movieApp.directive('spaceManager', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        replace: true,
        scope: {
            ngModel: '=ngModel'
        },
        link: function compile(scope, element, attrs, controller) {
            scope.$watch('ngModel', function (value) {
                if (!value)
                    return;
                var output = "";
                for (var i = 0; i < value.length; i++)
                {
                    if (i > 0 && (value[i] == "-" || value[i] == "_") && value[i - 1] != " ")
                        output = output + " ";
                    else
                    {
                        if (i > 0 && value[i] == value[i].toUpperCase() && value[i-1] != value[i-1].toUpperCase())
                            output = output + " ";
                    
                        output = output + value[i];
                    }
                }
                element.html(output);
            });
        }
    };
})

movieApp.filter('highlight', function($sce) {
	return function(title, phrase) {
		if (phrase)
			title = title.replace(new RegExp('('+phrase+')', 'gi'),'<span class="highlighted">$1</span>');

		return $sce.trustAsHtml(title);
	}
})