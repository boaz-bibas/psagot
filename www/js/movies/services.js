﻿/// <reference path="../angular.js" />
/// <reference path="module.js" />

movieApp.factory('MovieService', ['$window', '$http', '$q', function ($window, $http, $q) {
    var result = {};

    var searchMovies = function (searchTerm) {
		if (__env.apiUrl === "")
			$window.alert("Please set __env.apiUrl (on index.html)!!");
		
        var deferredAbort = $q.defer();

        var request = $http({
            method: "get",
            url: __env.apiUrl + "/movies/search/?query=" + searchTerm,
            timeout: deferredAbort.promise
        });

        var promise = request.then(
            function (response) {
                return (response);
            },
            function (response) {
                return ($q.reject());
            }
        );

        promise.abort = function () {
            deferredAbort.resolve();
        };

        promise.finally(
            function () {
                promise.abort = angular.noop;
                deferredAbort = request = promise = null;
            }
        );

        return (promise);
    }

    return {
        searchMovies: searchMovies
    };
}]);