﻿/// <reference path="../angular.js" />
/// <reference path="module.js" />

movieApp.controller("searchController", function ($scope, MovieService) {
    $scope.searchTerm = "";
    $scope.movies = [];
    $scope.showResults = false;
    $scope.loading = false;
	$scope.matchClass = 'term';

    var searchRequest = null;

    $scope.searchMovies = function (form) {
        if (form.$valid) {
            $scope.loading = true;

            if (searchRequest != null)
                searchRequest.abort();

            (searchRequest = MovieService.searchMovies($scope.searchTerm)).then(function (response) {
                $scope.movies = response.data;
                $scope.showResults = true;
                $scope.loading = false;
                form.$submitted = false;
            }, function (reason) {
                $scope.loading = false;
				$scope.movies = [];
                $scope.showResults = true;
                onError(reason);
            });
        }
    };
	
	$scope.emphasizeTerm = function(title) {
		var re = new RegExp($scope.searchTerm, 'g');
		return title;
	}

    var onError = function (reason) {
        console.log("ERROR: " + reason.data);
    }
});