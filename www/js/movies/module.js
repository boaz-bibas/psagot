﻿/// <reference path="../angular.js" />
"use strict";

var env = {};

if (window){  
  Object.assign(env, window.__env);
}

var movieApp = angular.module('movieSearch', [])
	.constant('__env', env)
	.filter('highlight', function($sce) {
		return function(title, phrase) {
			if (phrase)
				title = title.replace(new RegExp('('+phrase+')', 'gi'),'<span class="highlighted">$1</span>');

			return $sce.trustAsHtml(title);
		};
})