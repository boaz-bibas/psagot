﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PsagotAPI.Contracts
{
    public interface IMoviesSearch
    {
        Task<IEnumerable<MovieSearchResult>> SearchMovies(string query);
    }

    public class MovieSearchResult
    {
        public string Title { get; set; }
        public decimal Popularity { get; set; }
        public string ImgUrl { get; set; }
        public string ReleaseDate { get; set; }
    }
}