﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Psagot.Core;
using PsagotAPI.Contracts;

namespace PsagotAPI.Services
{
    public class MoviesSearchService : IMoviesSearch
    {
        private readonly string apiKey = "04a7e6bfe2911364293fe6ea4255ff36";
        private readonly string searchApiEndpoint = "https://api.themoviedb.org/3/search/movie";
        private readonly string imageApiEndpoint = "https://image.tmdb.org/t/p/w500";
        public async Task<IEnumerable<MovieSearchResult>> SearchMovies(string query)
        {
            List<MovieSearchResult> result = new List<MovieSearchResult>();

            try
            {
                string url = $"{searchApiEndpoint}?api_key={apiKey}&query={query}";
                MovieDBSearchREsult searchResult = await WebHelper.GetWebGetRequestTypesResponse<MovieDBSearchREsult>(url, new JsonSerializerSettings { DateFormatString = "yyyy-mm-dd" });
                foreach (MovieDBMovie movie in searchResult.Results)
                {
                    result.Add(movie.ToMovieSearchResult(imageApiEndpoint));
                }

                return result;
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public void Downloaded(object sender, DownloadStringCompletedEventArgs e)
        {
            string a = e.Result;
        }

        private class MovieDBMovie
        {
            [JsonProperty("title")]
            public string Title { get; set; }

            [JsonProperty("popularity")]
            public decimal Popularity { get; set; }

            [JsonProperty("poster_path")]
            public string PosterUrl { get; set; }

            [JsonProperty("release_date")]
            public DateTime? ReleaseDate { get; set; }

            public MovieSearchResult ToMovieSearchResult(string imgPath)
            {
                return new MovieSearchResult {
                    Title = this.Title,
                    Popularity = this.Popularity,
                    ImgUrl = String.IsNullOrEmpty(this.PosterUrl)  ? "" : $"{imgPath}{this.PosterUrl}",
                    ReleaseDate = this.ReleaseDate.HasValue ? this.ReleaseDate.Value.ToString("MMM dd, yyyy") : ""
                };
            }
        }

        private class MovieDBSearchREsult
        {
            [JsonProperty("total_results")]
            public int TotalResults { get; set; }

            [JsonProperty("total_pages")]
            public int TotalPages { get; set; }

            [JsonProperty("page")]
            public int Page { get; set; }

            [JsonProperty("results")]
            public List<MovieDBMovie> Results { get; set; } = new List<MovieDBMovie>();            
        }
    }
}