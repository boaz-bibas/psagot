﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PsagotAPI.Services
{
    public sealed class DataServicesGateway
    {
        private static readonly DataServicesGateway _instance = new DataServicesGateway();
        private MoviesSearchService _moviesSearchService { set; get; }

        static DataServicesGateway()
        {
        }

        private DataServicesGateway()
        {
            _moviesSearchService = new MoviesSearchService();
        }

        public static DataServicesGateway Instance
        {
            get
            {
                return _instance;
            }
        }

        public MoviesSearchService MoviesSearchService { get { return _moviesSearchService; } }
    }
}