﻿using Psagot.Core;
using PsagotAPI.Contracts;
using PsagotAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace PsagotAPI.Controllers
{
    public class MoviesController : ApiController
    {
        [HttpGet]
        public async Task<IHttpActionResult> Search(string query)
        {
            Logger.Info($"MoviesController.Search({query}) started");

            if (String.IsNullOrEmpty(query))
                return BadRequest("query is missing");

            query = query.Trim();
            if (query.Length < 2)
                return BadRequest("query must contain atleast 2 characters");

            IEnumerable<MovieSearchResult> movies = null;
            try
            {
                movies = await DataServicesGateway.Instance.MoviesSearchService.SearchMovies(query);
            }
            catch (Exception ex)
            {
                Logger.Error($"MoviesController.Search({query}) failed", ex);
                return InternalServerError();
            }

            if (movies.Count() == 0)
                return NotFound();

            Logger.Info($"MoviesController.Search({query}) ended");
            return Ok(movies);
        }
    }
}
