﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Psagot.Core
{
    public class Logger
    {
        private static log4net.ILog log { get; set; }

        static Logger()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger("Logger");
        }

        public static void Error(string msg, Exception ex)
        {
            LoggerMessage message = new LoggerMessage(LogTypes.Error);
            if (!String.IsNullOrEmpty(msg))
                message.Log.Message = msg;
            else
                message.Log.Message = ex.Message;
            if (ex != null)
                message.Log.Number = ex.HResult;
            message.Log.RawData = ExceptionDetails(ex);

            log.Error(message, ex);
        }

        public static void Error(Exception ex)
        {
            Error(String.Empty, ex);
        }

        public static void Error(string msg)
        {
            Error(msg, null);
        }

        public static void InternalError(LoggerMessage message, Exception ex)
        {
            log.Error(message, ex);
        }

        public static void Info(string msg)
        {
            LoggerMessage message = new LoggerMessage(LogTypes.Info);
            message.Log.Message = msg;

            log.Info(message);
        }

        public static void Warn(string msg)
        {
            LoggerMessage message = new LoggerMessage(LogTypes.Warn);
            message.Log.Message = msg;

            log.Warn(message);
        }

        public static void Debug(string msg)
        {
            LoggerMessage message = new LoggerMessage(LogTypes.Debug);
            message.Log.Message = msg;

            log.Debug(message);
        }

        #region == private methonds =================================            

        private static string ExceptionDetails(Exception ex)
        {
            if (ex == null) return "";

            string className = "";
            string methodName = "";
            string lineNumber = "";
            string columnNumber = "";

            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
            System.Diagnostics.StackFrame frame = trace.GetFrame(0);
            if (frame != null)
            {
                System.Reflection.MethodBase method = frame.GetMethod();

                methodName = method.ToString();
                lineNumber = frame.GetFileLineNumber().ToString();
                columnNumber = frame.GetFileColumnNumber().ToString();

                if (method.ReflectedType != null)
                    className = method.ReflectedType.FullName;
            }

            if (ex.InnerException == null)
                return String.Format("EXCEPTION MESSAGE: '{0}', CLASS: '{1}', METHOD: '{2}' LINE: '{3}', COLUMN: '{4}'", ex.Message, className, methodName, lineNumber, columnNumber);
            else
                return String.Format("EXCEPTION MESSAGE: '{0}', CLASS: '{1}', METHOD: '{2}' LINE: '{3}', COLUMN: '{4}'\nINNER EXCEPTION: {5}", ex.Message, className, methodName, lineNumber, columnNumber, ex.InnerException.Message);
        }
        #endregion == private methonds =================================


        #region == classes =======================================
        public class LoggerMessage
        {
            public LogTypes Type { get; set; }
            public Log Log { get; set; }

            public LoggerMessage()
            {
                Type = LogTypes.Unknown;
                Log = new Log();
            }

            public LoggerMessage(LogTypes type)
                : this()
            {
                Type = type;
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("---------------- {0} -----------------", this.Type.ToString()).AppendLine();
                sb.AppendFormat("Log:: Number: [{0}] Message: [{1}] RawData: [{2}] AdditionalData: [{3}]\n", Log.Number, Log.Message, Log.RawData, Log.AdditionalData);
                sb.Append("-----------------------------------------------");

                return sb.ToString();
            }
        }

        public class Log
        {
            public string AdditionalData { get; set; }
            public string Message { get; set; }
            public string RawData { get; set; }
            public int Number { get; set; }
        }

        public enum LogTypes
        {
            Unknown = 0,
            Error = 1,
            Warn = 2,
            Info = 3,
            Debug = 4,
            InternalError = 5
        }
        #endregion == classes =======================================
    }
}