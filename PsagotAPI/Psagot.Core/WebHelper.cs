﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Psagot.Core
{
    public static class WebHelper
    {
        public static async Task<string> GetWebGetRequestStringResponse(string url)
        {
            string response = String.Empty;
            try
            {
                using (WebClient client = new WebClient { Encoding = Encoding.UTF8 })
                {
                    client.Headers.Add(HttpRequestHeader.Accept, "application/json");
                    client.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36");

                    response = await client.DownloadStringTaskAsync(new Uri(url));
                }
            }
            catch (WebException ex)
            {
                string a = ex.Message;
            }
            catch (Exception ex)
            {
                string a = ex.Message;
            }

            return response;
        }

        public static async Task<T> GetWebGetRequestTypesResponse<T>(string url) where T : class
        {
            return await GetWebGetRequestTypesResponse<T>(url, null);
        }

        public static async Task<T> GetWebGetRequestTypesResponse<T>(string url, JsonSerializerSettings settings) where T : class
        {
            string response = await GetWebGetRequestStringResponse(url);
            return JsonConvert.DeserializeObject<T>(response, settings);
        }
    }
}